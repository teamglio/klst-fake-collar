require 'socket'
require_relative 'nmea_parser.rb'
require 'date'

class KLSTFakeCollar
  def initialize(path_to_gps_log, device_id, collar_server_hostname, collar_server_port, simulation_delay, write_retries)
    @path_to_gps_log = path_to_gps_log
    @device_id = device_id
    @collar_server_hostname = collar_server_hostname
    @collar_server_port = collar_server_port
    @simulation_delay = simulation_delay
    @write_retries = write_retries
  end

  def simulate
    nmea_parser = NMEAParser.new
    File.open(@path_to_gps_log, "r").each_line do |line|
      location = nmea_parser.parse_NMEA(line)
      if location[:last_nmea] == "RMC"
        puts "Device #{@device_id}: Opening socket"
        @write_retries.times do |i|
          socket = TCPSocket.open(@collar_server_hostname, @collar_server_port)
          if location[:lat_ref] == "N"
            lat = location[:latitude].to_s
          elsif location[:lat_ref] == "S"
            lat = "-" + location[:latitude].to_s
          end
          if location[:long_ref] == "E"
            lng = location[:longitude].to_s
          elsif location[:long_ref] == "W"
            lng = "-" + location[:longitude].to_s
          end          
          char = socket.write("#{@device_id}, #{DateTime.now.to_s}, #{lat}, #{lng}") if socket.gets == "Ready\n"        
          puts "Device #{@device_id}: Write attempt #{i + 1} of #{@write_retries}: #{char} characters written" unless char.nil?
          break unless char.nil?
          puts "Device #{@device_id}: Retrying..."
        end
        puts "Device #{@device_id}: Waiting for #{@simulation_delay} seconds"
        sleep(@simulation_delay)
      end

    end
  end

end

# Can change
simulation_delay = 1 # How long the simulator must wait before sending the next position - this is used to simulate movement
write_retries = 20 # How many times the simulator must retry writing to the socket

# Don't change
collar_server_hostname = "21340.b4c9b99a-a7a5-40b2-8527-860da9d9a6aa.sockets.ruppells.io"
collar_server_port = 21340


# Fake Collar 1
@fc1 = KLSTFakeCollar.new("/vagrant/glio/klst/klst_fake_collar/gps_logs/zoo_walk.txt", 123, collar_server_hostname, collar_server_port, simulation_delay, write_retries)

# Fake Collar 2
@fc2 = KLSTFakeCollar.new("/vagrant/glio/klst/klst_fake_collar/gps_logs/stockholm_walk.txt", 567, collar_server_hostname, collar_server_port, simulation_delay, write_retries)

# Fake Collar 3
@fc3 = KLSTFakeCollar.new("/vagrant/glio/klst/klst_fake_collar/gps_logs/peregrine.txt", 891, collar_server_hostname, collar_server_port, simulation_delay, write_retries)

# Fake Collar 4
@fc4 = KLSTFakeCollar.new("/vagrant/glio/klst/klst_fake_collar/gps_logs/roodeberg_drive.txt", 321, collar_server_hostname, collar_server_port, simulation_delay, write_retries)


puts "Simulation started at #{Time.now}"
t1 = Thread.new { @fc1.simulate }
t2 = Thread.new { @fc2.simulate }
t3 = Thread.new { @fc3.simulate }
t4 = Thread.new { @fc4.simulate }
t1.join
t2.join
t3.join
t4.join
puts "Simulation ended at #{Time.now}"

